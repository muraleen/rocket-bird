package com.me.rocketbird;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.WindowManager;

import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;

public class MainActivity extends AndroidApplication {
	
	protected static final String TAG = "Rocket Birds";
	IabHelper mHelper;
	
	IabHelper.QueryInventoryFinishedListener mGotInventoryListener = new IabHelper.QueryInventoryFinishedListener() {
        public void onQueryInventoryFinished(IabResult result, Inventory inventory) {
            Log.d(TAG, "Query inventory finished.");
            if (result.isFailure()) {
                Log.d(TAG, "Failed to query inventory: " + result);
                return;
            }

            Log.d(TAG, "Query inventory was successful.");
            
            for(Purchase purchase: inventory.getAllPurchases()) {
            	Log.d(TAG, "Found Purchase: " + purchase.getSku());
            	try {
					mHelper.consume(purchase);
					Log.d(TAG, "Consuming: " + purchase.getSku());
				} catch (IabException e) {
					e.printStackTrace();
				}
            }
        }
    };
	
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        
        AndroidApplicationConfiguration cfg = new AndroidApplicationConfiguration();
        cfg.useGL20 = false;
        cfg.useAccelerometer = true; // Switch to true later
        
        // Public Key
		
 		String base64EncodedPublicKey = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAivAljpoAU9qlNaThc2AsP9itPIjqLTxC8bDpBk4NK2T2eZYm64MTi9JMsBYefFkayMpdTZ0xJVCL1paJvT1zCdnkc1C7Xaa6IqxL03NLgcID/9qLwfDfp3ijpTe6YIG2ckf6277AXAvvsZG7Ro6rlZ5a+laadoyYSu8u2rhhd//qfBEtLBX0O3tQFVyq3BEugmIABoZaKfXNaEewBJXGhG297EhTJ3244N1J93xHnPGlL7abxYO3bh7v0n2VawxwY9s2nRSDdGPt3J1Xp7lU9GEo+0egZ4Ffm2MVt5L/giZxlTH+tBIc6OZeVfXF6s+8BcUZhXYWPJ06My74+FVyUwIDAQAB";
 		
 		mHelper = new IabHelper(this, base64EncodedPublicKey);
 		
 		mHelper.startSetup(new IabHelper.OnIabSetupFinishedListener() {
			
			@Override
			public void onIabSetupFinished(IabResult result) {
				if(!result.isSuccess()) {
					Log.d(TAG, "Problem setting up In-app Billing: " + result);
				}
				mHelper.queryInventoryAsync(mGotInventoryListener);
			}
		});
        
        initialize(new Rocket_Bird(mHelper, this), cfg);
        
        // Prevent Phone from going into sleep mode
        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }
    
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    Log.d(TAG, "onActivityResult(" + requestCode + "," + resultCode + "," + data);

	    // Pass on the activity result to the helper for handling
	    if (!mHelper.handleActivityResult(requestCode, resultCode, data)) {
	        // not handled, so handle it ourselves (here's where you'd
	        // perform any handling of activity results not related to in-app
	        // billing...
	        super.onActivityResult(requestCode, resultCode, data);
	    }
	    else {
	        Log.d(TAG, "onActivityResult handled by IABUtil.");
	    }
	}
    
    @Override
    public void onDestroy() {
       super.onDestroy();
       if (mHelper != null) mHelper.dispose();
       mHelper = null;
    }
}