package com.me.rocketbird;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;

public class Main {
	public static void main(String[] args) {
		LwjglApplicationConfiguration cfg = new LwjglApplicationConfiguration();
		cfg.title = "Rocket_Bird";
		cfg.useGL20 = false;
		cfg.width = 540;
		cfg.height = 960;
		
		new LwjglApplication(new Rocket_Bird(null, null), cfg);
	}
}
