package com.me.rocketbird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class Scroller {

	Vector2 pos, size;
	Sprite background;
	
	public Scroller(Vector2 pos, Vector2 size, String textureLoc) {
		this.pos = pos;
		this.size = size;
		background = new Sprite(new Texture(Gdx.files.internal(textureLoc)));
		
	}
	
	public void scroll(SpriteBatch batch, float scr_height, float rate, boolean paused) {
		if(!paused) {
			pos.y -= Gdx.graphics.getDeltaTime()*rate*(scr_height/960);
			if(pos.y <= -size.y) {
				pos.y = 0;
			}
		}
		batch.draw(background, pos.x, pos.y, size.x, size.y);
		batch.draw(background, pos.x, pos.y + size.y, size.x, size.y);
	}
	
}
