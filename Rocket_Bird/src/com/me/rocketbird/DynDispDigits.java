package com.me.rocketbird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class DynDispDigits {

	Vector2 position1, position2, position, size;
	
	int p1_x, p1_y;
	
	Texture digits;
	Texture ft;
	Sprite ftSprite;
	TextureRegion[] frames;
	boolean FromLeft = true;
	int rate;
	
	String textureLoc, suffixLoc;
	
	int[] digit = {10,10,10,10,10,10,10,10,10,10,10,10,10};
	
	public DynDispDigits(Vector2 position1, Vector2 position2, Vector2 size, String textureLoc, String suffixLoc, boolean FromLeft, int rate) {

		this.position1 = position1;
		this.position2 = position2;
		this.position = position1;
		this.size = size;
		this.rate = rate;
		digits = new Texture(Gdx.files.internal(textureLoc));
		TextureRegion[][] tmp = TextureRegion.split(digits, digits.getWidth()/10, digits.getHeight());
		frames = new TextureRegion[10];
		this.FromLeft = FromLeft;
		p1_x = (int) position1.x;
		p1_y = (int) position1.y;
		
		ft = new Texture(Gdx.files.internal(suffixLoc));
		ftSprite = new Sprite(ft);
		
		int index = 0;
		
		for(int i = 0; i < 10; i++) {
				frames[index++] = tmp[0][i];
		}
	}
	// Equations from Right: X_i = X_0 + Wd(i-l).
	
	public void display(SpriteBatch batch, int n) { // Similar to render function
		
		position.x = approachExpo(position.x,position2.x,rate);
		position.y = approachExpo(position.y,position2.y,rate);
		
		// Find number of digits to display
		int length = (int) (Math.log10(n));
		
		if(n < 1) {

			batch.draw(frames[0], position.x, position.y, size.x, size.y);
			
		} else {
			
			for(int i = 0; i <= length; i++) {
				digit[i] = (int) (n/Math.pow(10,(length - i)));
				for(int j = 0; j < i; j++) {
					digit[i] -= (int) ((digit[j]*Math.pow(10,(length - j))))/Math.pow(10,(length - i));
				}
				
				if(FromLeft){
					batch.draw(frames[digit[i]], position.x + (i*size.x*3)/4, position.y, size.x, size.y);
				} else {
					batch.draw(frames[digit[i]], position.x + (size.x*(i-length - 1)), position.y, size.x, size.y);
				}
				
			
			}
			
		}
			
		if(FromLeft){
			batch.draw(ftSprite,position.x + 5 + (size.x*3*(length+1))/4, position.y, size.x, size.x);
		} else {
			batch.draw(ftSprite,position.x - ((length + 1)*size.x) - size.y, position.y, size.y, size.y);
		}
				
	}
	
	public void reset() {
		position.x = p1_x;
		position.y = p1_y;
	}
	
	// Approach target value smoothly
	public float approachExpo(float value, float target, float rate) {
		if(target > value) {
			return value + (target-value)/rate;
		} else if(target < value) {
			return value - (value-target)/rate;
		} else {
			return value;
		}
	}
	
}
