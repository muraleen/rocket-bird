package com.me.rocketbird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class StaticImg {
	
	Vector2 position, size;
	String textureLoc;
	Texture texture;
	Sprite sprite;
	
public StaticImg(Vector2 position, Vector2 size, String textureLoc){
		
		this.position = position;
		this.size = size;
		texture = new Texture(Gdx.files.internal(textureLoc));
		sprite = new Sprite(texture);
		
	}
	
	public void render(SpriteBatch batch) {
		batch.draw(sprite, position.x, position.y, size.x, size.y);
	}

}
