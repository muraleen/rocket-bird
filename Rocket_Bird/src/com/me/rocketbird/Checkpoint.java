package com.me.rocketbird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class Checkpoint {
	
	Vector2 pos, size;
	int speed;
	Animation animation;
	Texture flag;
	boolean added = false;
	TextureRegion[] frames;
	
	public Checkpoint(Vector2 pos, Vector2 size, String textureLoc) {
		
		this.pos = pos;
		this.size = size;
		flag = new Texture(Gdx.files.internal(textureLoc));
		TextureRegion[][] tmp = TextureRegion.split(flag, flag.getWidth()/2, flag.getHeight());
		frames = new TextureRegion[2];
		
		int index = 0;
		
		for (int j = 0; j < 2; j++) {
			frames[index++] = tmp[0][j];
		}
		
		animation = new Animation(1f, frames);
	}
	
	public void render(SpriteBatch batch, float state, float scr_height) {
		pos.y -= Gdx.graphics.getDeltaTime()*400*(scr_height/960);
		batch.draw(animation.getKeyFrame(state), pos.x, pos.y, size.x, size.y);
	}

}
