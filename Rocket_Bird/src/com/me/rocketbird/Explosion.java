package com.me.rocketbird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class Explosion {
	
	Vector2 position, size;
	
	private static final int animCol = 8;
	private static final int animRow = 8;
	
	Animation animation;
	TextureRegion[] frames;
	TextureRegion currentFrame;
	float stateTime;
	Texture texture;
	
	boolean onScreen = true;
	
public Explosion(Vector2 position, Vector2 size, Texture texture){
	
		this.texture = texture;
		this.position = position;
		this.size = size;
		TextureRegion[][] tmp = TextureRegion.split(texture, texture.getWidth()/animCol, texture.getHeight()/animRow);
		frames = new TextureRegion[animCol * animRow];
		
		int index = 0;
		
		for(int i = 0; i < animRow; i++) {
			for (int j = 0; j < animCol; j++) {
				frames[index++] = tmp[i][j];
			}
		}
		
		animation = new Animation(0.009f, frames);
		stateTime = 0.01f;
		currentFrame = animation.getKeyFrame(0);
		
	}
	
	public void update() {
		
		if(stateTime < 0.57) {
			stateTime += Gdx.graphics.getDeltaTime();
		} else {
			onScreen = false;
		}
		position.y += 2;
		currentFrame = animation.getKeyFrame(0 + stateTime);
	}
	
	public void render(SpriteBatch batch) {
		batch.draw(currentFrame, position.x, position.y, size.x, size.y);
	}
}
