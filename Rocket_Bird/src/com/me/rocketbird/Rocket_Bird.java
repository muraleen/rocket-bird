package com.me.rocketbird;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.Random;

import android.content.Intent;
import android.os.Handler;
import android.util.Log;
import android.widget.Toast;

import com.badlogic.gdx.ApplicationListener;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.audio.Music;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.GL10;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.Texture.TextureFilter;
import com.badlogic.gdx.graphics.Texture.TextureWrap;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Timer;
import com.badlogic.gdx.utils.Timer.Task;

public class Rocket_Bird implements ApplicationListener, InputProcessor {
	
	protected static final String TAG = "Rocket Birds";
	int gameState = 0; // 0-INTRO, 1-MENU, 2-STREET, 3-JUNGLE
	boolean stateInit = false;
	boolean useAccelerometer = true;
	boolean goSoundPlayed;
	int turnSpd = 0, scoreUpCounter = 0;
	
	Scroller background;
	
	boolean toastCreated = false;
	
	Checkpoint checkpoint;
	
	boolean paused = false;
	
	int displayedScore = 0;
	// long tallyLoopId = 0;
	boolean dingPlayed = false;
	boolean tallyLoopStarted = false;
	
	boolean themeStopped = false;
	
	boolean settingsMode = false, howToPlayMode = false, storeMode = false;
	boolean themeStarted = false;
	
	ArrayList<Music> theme = new ArrayList<Music>();
	
	int tmpPos1, tmpPos2;
	
	int numRockets;
	
	DispDigits score, nRockets, nest;
	DynDispDigits dist, best;
	
	// Keep track of distance flown (score)
	int distance = 0, checkpoints = 0;
	float frameCounter = 0f;
	
	SpriteBatch batch;
	Texture bird, menuBg, loading, explosionTexture, rocketTexture, collectRocket;
	
	ArrayList<Texture> cars = new ArrayList<Texture>();
	ArrayList<Texture> boats = new ArrayList<Texture>();
	
	DynamicImg gameMenu, title;
	Bird activeBird;
	dynamicAnimation menuBird;
	staticAnimation logo;
	ArrayList<Explosion> explosions = new ArrayList<Explosion>();
	StaticImg ctlButtons, rocketButton;
	DynamicImg gameOver, goMenu, settingsBg, howToPlayBg, startRun, pauseMenu;
	DynamicImg2 storeBg;
	Clickable playOn, pack3, pack10, pack20, prun5 ,streets, waters, moveLeft, moveRight, shootRocket, replay, home, settingsButton, settingsButton2, htpBtn, htpBtn2, homeBtn, resumeBtn, storeBtn, storeBtn2;
	Sprite menuBgSprite, loadingSprite;
	ArrayList<Collectable> collectables = new ArrayList<Collectable>();
	
	DynamicCheckButton musicBtn, sfxBtn, hiResBtn, ctlBtn;
	DynamicSlider accelSlider;
	float musicVol = 0, sfxVol = 0;
	
	float scr_width;
	float scr_height;
	Vector2 bird_size;
	int obsId; // Keep track of number of obstacles
	
	// Keep track of some data locally
	LocalData data, settings, purchases;
	
	ArrayList<Obstacle> obstacles = new ArrayList<Obstacle>();
	ArrayList<Projectile> rockets = new ArrayList<Projectile>();
	
	float scrollTimer = 0.0f;
	float gameTimer = 0.0f;
	float gameoverTimer = 0.0f;
	float logoTimer = 0.0f;
	int entryRate = 15;
	float speed = 7;
	float rocketLoadTimer = 0.0f, menuLoadTimer = 0.0f;
	
	float bgScrollRate = 400;
	
	boolean collided = false;
	
	ArrayList<Sound> sounds = new ArrayList<Sound>();
	Sound splash;
	
	// Start positions for obstacles
	
	int[] obsCenter = {232, 540, 840}; // On a scale of of 1024/scr_width
	
	IabHelper mHelper;
	MainActivity activity;
	
	public Rocket_Bird(IabHelper helper, MainActivity mainActivity) {
		mHelper = helper;
		activity = mainActivity;
	}
	
	// Handle In-app Purchases	
	IabHelper.OnConsumeFinishedListener mConsumeFinishedListener = new IabHelper.OnConsumeFinishedListener() {
		public void onConsumeFinished(Purchase purchase, IabResult result) {
			if(result.isFailure()) {
				Log.d(TAG, "Error purchasing: " + result);
				return;
			}
			if(purchase.getSku().equals("android.test.purchased")) {
				// Credit 3 rockets to the user's inventory
				purchases.Data[0] += 3;
				try {
					LocalData.saveData(purchases, "rocketbird/data/purchases.dat");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if(purchase.getSku().equals("rocket_pack_3")) {
				// Credit 3 rockets to the user's inventory
				purchases.Data[0] += 3;
				try {
					LocalData.saveData(purchases, "rocketbird/data/purchases.dat");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if(purchase.getSku().equals("rocket_pack_10")) {
				// Credit 10 rockets to the user's inventory
				purchases.Data[0] += 10;
				try {
					LocalData.saveData(purchases, "rocketbird/data/purchases.dat");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if(purchase.getSku().equals("rocket_pack_20")) {
				// Credit 20 rockets to the user's inventory
				purchases.Data[0] += 20;
				try {
					LocalData.saveData(purchases, "rocketbird/data/purchases.dat");
				} catch (IOException e) {
					e.printStackTrace();
				}
			} else if(purchase.getSku().equals("rockets_5_per_run")) {
				// Start with 5 rockets
				purchases.Data[1] = 1;
				try {
					LocalData.saveData(purchases, "rocketbird/data/purchases.dat");
				} catch (IOException e) {
					e.printStackTrace();
				}
				
			} else if(purchase.getSku().equals("keep_playing")) {
				if(purchases.Data[1] == 1) {
					numRockets = 5; // Start with 5 rockets
				} else {
					numRockets = 3; // Start with 3 rockets
				}
				checkpoint.added = false;
				gameTimer = 0.0f;
				gameoverTimer = 0.0f;
				goMenu.reset();
				dist.reset();
				best.reset();
				gameOver.reset();
				rocketLoadTimer = -0.5f;
				collided = false; 
				bgScrollRate = 400;
				frameCounter = 0f;
				distance = 0;
				for(Obstacle obs: obstacles) {
					obs.onScreen = false;
				}
				title.reset();
				gameMenu.reset();
				menuBird.reset();
				activeBird.burstTime = 0f;
				displayedScore = 0;
				dingPlayed = false;
				sounds.get(7).stop();
				tallyLoopStarted = false;
				theme.get(0).play();
				startRun.reset();
				theme.get(0).setVolume(musicVol);
				theme.get(1).setVolume(musicVol);
				themeStopped = false;
				checkpoint.pos.y = scr_height;
			}
		}
	};
	
	IabHelper.OnIabPurchaseFinishedListener mPurchaseFinishedListener = new IabHelper.OnIabPurchaseFinishedListener() {
		public void onIabPurchaseFinished(IabResult result, Purchase purchase) {

			if(result.isFailure()) {
				Log.d(TAG, "Error purchasing: " + result);
				activity.runOnUiThread(new Runnable() {
					public void run() {
						Toast.makeText(activity, "Purchase Cancelled...", Toast.LENGTH_SHORT).show();
					}
				});
				return;
			}
			// Consume purchased item
			mHelper.consumeAsync(purchase, mConsumeFinishedListener);
			activity.runOnUiThread(new Runnable() {
				public void run() {
					Toast.makeText(activity, "Please wait for a few seconds...", Toast.LENGTH_LONG).show();
				}
			});
		}
	};

	@Override
	public void create() {
		
		// Get Screen Resolution for auto-scaling
		
		scr_width = Gdx.graphics.getWidth();
		scr_height = Gdx.graphics.getHeight();
		
		// Handle Inputs
		
		Gdx.input.setInputProcessor(this);
		Gdx.input.setCatchBackKey(true);
		
		// Create Sprite Batch
		
		batch = new SpriteBatch();
		
		// Load Purchases
		
		int[] purchasesArray = {0, 0};
		purchases = new LocalData(purchasesArray);
		
		if(Gdx.files.local("rocketbird/data/purchases.dat").exists()) {
			try {
				purchases = LocalData.readData("rocketbird/data/purchases.dat");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				LocalData.saveData(purchases, "rocketbird/data/purchases.dat");
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
		
		// Load Saved Data
		
		int[] bestArray = {0, 0};
		data = new LocalData(bestArray);
		
		if(Gdx.files.local("rocketbird/data/local.dat").exists()) {
			try {
				data = LocalData.readData("rocketbird/data/local.dat");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				LocalData.saveData(data, "rocketbird/data/local.dat");
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
		
		// Load Saved Settings
		
		int[] settingsArray = {1, 1, 1, 1, 100};
		settings = new LocalData(settingsArray);
		
		if(Gdx.files.local("rocketbird/data/settings.dat").exists()) {
			try {
				settings = LocalData.readData("rocketbird/data/settings.dat");
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else {
			try {
				LocalData.saveData(settings, "rocketbird/data/settings.dat");
			} catch(IOException e) {
				e.printStackTrace();
			}
		}
		
		musicVol = settings.Data[0];
		sfxVol = settings.Data[1];
		if(settings.Data[3] == 1) {
			useAccelerometer = true;
		} else {
			useAccelerometer = false;
		}
			
	}

	@Override
	public void dispose() {
		for(Sound sound: sounds) {
			sound.stop();
			sound.dispose();
		}
		for(Music music: theme) {
			music.stop();
			music.dispose();
		}
		// splash.dispose();
		try {
			LocalData.saveData(settings, "rocketbird/data/settings.dat");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public void render() {		
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL10.GL_COLOR_BUFFER_BIT);
		
		batch.begin();
		
		if(gameState == 0) { // INTRO MODE - show company logo
			
			if(stateInit) {
				logoTimer += Gdx.graphics.getDeltaTime();
				logo.update();
				logo.render(batch);
				if(logoTimer > 5f) {
					batch.draw(loadingSprite, 0, (scr_height/2) - (scr_width/8), scr_width, scr_width/2);
					stateInit = false;
					gameState = 1;
					
					// Load all sounds here while the loading screen on still on
					
					// Collect Rocket Texture
					collectRocket = new Texture(Gdx.files.internal("projectile/collect-rocket.png"));
					
					// Rocket sound at ID = 0
					sounds.add(Gdx.audio.newSound(Gdx.files.internal("soundfx/rocket-launch.mp3")));
					// Game over sound at ID = 1
					sounds.add(Gdx.audio.newSound(Gdx.files.internal("soundfx/gameover.mp3")));
					// Explosion sound at ID = 2
					sounds.add(Gdx.audio.newSound(Gdx.files.internal("soundfx/explosion.mp3")));
					// +1 Rocket sound at ID = 3
					sounds.add(Gdx.audio.newSound(Gdx.files.internal("soundfx/collectRocket.mp3")));
					// End Swoosh sound at ID = 4
					sounds.add(Gdx.audio.newSound(Gdx.files.internal("soundfx/swoosh.mp3")));
					// Beat High Score sound at ID = 5
					sounds.add(Gdx.audio.newSound(Gdx.files.internal("soundfx/highscore.mp3")));
					// Splash Screen at ID = 6
					sounds.add(Gdx.audio.newSound(Gdx.files.internal("soundfx/splash.mp3")));
					// Score Tally up at ID = 7
					sounds.add(Gdx.audio.newSound(Gdx.files.internal("soundfx/tally.wav")));
					// Score Tally End at ID = 8
					sounds.add(Gdx.audio.newSound(Gdx.files.internal("soundfx/tally-end.mp3")));
					// Checkpoint sound at ID = 9
					sounds.add(Gdx.audio.newSound(Gdx.files.internal("soundfx/checkpoint.mp3")));
					
					// Pre-load textures
					for(int i = 0; i <= 6; i++) {
						cars.add(new Texture(Gdx.files.internal("obs/"+i+".png")));
					}
					for(int i = 0; i <= 5; i++) {
						boats.add(new Texture(Gdx.files.internal("obs/w"+i+".png")));
					}
					
					explosionTexture = new Texture(Gdx.files.internal("obs/explosion.png"));
					
					rocketTexture = new Texture(Gdx.files.internal("projectile/rocket-anim.png"));
					
					// Theme Intro at ID = 1
					theme.add(Gdx.audio.newMusic(Gdx.files.internal("music/intro.wav")));
					// Theme Loop at ID = 2
					theme.add(Gdx.audio.newMusic(Gdx.files.internal("music/loop.wav")));
					
					// theme.get(1).play();
					// theme.get(1).stop();
					
				}
			} else {
				if(settings.Data[2] == 1) {
					logo = new staticAnimation(new Vector2(0, (scr_height/2) - (scr_width/8)), new Vector2(scr_width, scr_width/2), "menu/logo-animation.png", 5.4f, 0.09375f);
				} else {
					logo = new staticAnimation(new Vector2(0, (scr_height/2) - (scr_width/8)), new Vector2(scr_width, scr_width/2), "menu/logo-animation-lores.png", 5.4f, 0.09375f);
				}
				loading = new Texture(Gdx.files.internal("menu/loading.png"));
				loadingSprite = new Sprite(loading);
				final Sound splash = Gdx.audio.newSound(Gdx.files.internal("soundfx/splash.mp3"));
				Timer.schedule(new Task() {
					@Override
					public void run() {
						splash.play(sfxVol);
					}
				}, 1);
				stateInit = true;
				
			}
		
		} else if(gameState == 1) { // GAME MENU
			
			if(stateInit) {
				
				/* if((!theme.get(0).isPlaying()) && (!themeStarted)) {
					theme.get(1).play();
					theme.get(1).setLooping(true);
					theme.get(1).setVolume(musicVol);
					themeStarted = true;
				} */
					
				if(!theme.get(0).isPlaying() && !theme.get(1).isPlaying()) {
					theme.get(1).play();
					theme.get(1).setLooping(true);
					theme.get(1).setVolume(musicVol);
					themeStarted = true;
				}
				
				scrollTimer += (Gdx.graphics.getDeltaTime()/20);
				if(scrollTimer > 1.0f) {
					scrollTimer = 0.0f;
				}
				
				menuBgSprite.setU2(scrollTimer);
				menuBgSprite.setU(scrollTimer + 1);
				
				// Draw background
				batch.draw(menuBgSprite, 0, 0, 0, 0, scr_width, scr_height, 4, 1, 0);
				
				// Draw Game menu
				gameMenu.render(batch);
				
				// Draw Title
				title.render(batch);
				
				menuBird.update();
				menuBird.render(batch);
				if(purchases.Data[1] == 1) {
					storeBg.render(batch, false);
				} else {
					storeBg.render(batch, true);
				}
				howToPlayBg.render(batch);
				settingsBg.render(batch);
				musicBtn.render(batch);
				sfxBtn.render(batch);
				hiResBtn.render(batch);
				ctlBtn.render(batch);
				accelSlider.render(batch);
				if((settings.Data[3] == 1) && (settingsBg.active)) {
					accelSlider.active = true;
				} else {
					accelSlider.active = false;
				}
				
				if(settingsMode) {
					
					if(settingsButton2.isClicked()) {
						settingsBg.active = false;
						musicBtn.active = false;
						sfxBtn.active = false;
						hiResBtn.active = false;
						ctlBtn.active = false;
						menuLoadTimer = 0.5f;
						settingsMode = false;
					}
					
					if(musicBtn.isClicked()) {
						musicBtn.checked = !musicBtn.checked;
						if(musicBtn.checked) {
							musicVol = 1;
						} else {
							musicVol = 0;
						}
						settings.Data[0] = (int) musicVol;
						theme.get(0).setVolume(musicVol);
						theme.get(1).setVolume(musicVol);
						try {
							LocalData.saveData(settings, "rocketbird/data/settings.dat");
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					if(sfxBtn.isClicked()) {
						sfxBtn.checked = !sfxBtn.checked;
						if(sfxBtn.checked) {
							sfxVol = 1;
						} else {
							sfxVol = 0;
						}
						settings.Data[1] = (int) sfxVol;
						try {
							LocalData.saveData(settings, "rocketbird/data/settings.dat");
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					if(hiResBtn.isClicked()) {
						hiResBtn.checked = !hiResBtn.checked;
						if(hiResBtn.checked) {
							settings.Data[2] = 1;
						} else {
							settings.Data[2] = 0;
						}
						try {
							LocalData.saveData(settings, "rocketbird/data/settings.dat");
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					if(ctlBtn.isClicked()) {
						ctlBtn.checked = !ctlBtn.checked;
						if(ctlBtn.checked) {
							settings.Data[3] = 1;
							useAccelerometer = true;
						} else {
							settings.Data[3] = 0;
							useAccelerometer = false;
						}
						try {
							LocalData.saveData(settings, "rocketbird/data/settings.dat");
						} catch (IOException e) {
							e.printStackTrace();
						}
					}
					if(settings.Data[3] == 1) {
						settings.Data[4] = (int) ((int) 200 * accelSlider.sliderValue());
					}
					
				} else if (howToPlayMode) {
					
					if(htpBtn2.isClicked()) {
						howToPlayBg.active = false;
						howToPlayMode = false;
					}
					
					if(settingsButton.isClicked()) {
						settingsBg.active = true;
						musicBtn.active = true;
						sfxBtn.active = true;
						hiResBtn.active = true;
						ctlBtn.active = true;
						menuLoadTimer = 0.5f;
						settingsMode = true;
						howToPlayBg.active = false;
						howToPlayMode = false;
					}
					
				} else if (storeMode) { 
					
					if(Math.abs(storeBg.position.x - storeBg.position2.x) < 20) {
						nest.display(batch, purchases.Data[0]);
					}
					
					if(storeBtn2.isClicked()) {
						storeMode = false;
						storeBg.active = false;
					}
					
					if(htpBtn.isClicked()) {
						howToPlayBg.active = true;
						howToPlayMode = true;
						menuLoadTimer = 0.5f;
						storeMode = false;
						storeBg.active = false;
					}
					
					if(settingsButton.isClicked()) {
						settingsBg.active = true;
						musicBtn.active = true;
						sfxBtn.active = true;
						hiResBtn.active = true;
						ctlBtn.active = true;
						menuLoadTimer = 0.5f;
						settingsMode = true;
						howToPlayBg.active = false;
						howToPlayMode = false;
						storeBg.active = false;
						storeMode = false;
					}
					
					if(Math.abs(storeBg.position.x - storeBg.position2.x) < 2) {
						
						if(pack3.isClicked()) {
							tryPurchase("rocket_pack_3");
						} else if(pack10.isClicked()) {
							tryPurchase("rocket_pack_10");
						} else if(pack20.isClicked()) {
							tryPurchase("rocket_pack_20");
						} else if(prun5.isClicked()) {
							if(purchases.Data[1] == 0) {
								tryPurchase("rockets_5_per_run");
							}
						}
						
					}
					
				} else {
					
					if(menuLoadTimer > 1) {
						
						if(streets.isClicked()) {
							stateInit = false;
							gameState = 2; // Switch to playing on the streets map with the accelerometer
							
							if(!useAccelerometer) {
								ctlButtons = new StaticImg(new Vector2(0, scr_width/50), new Vector2(scr_width, scr_width/8), "buttons/ctlButtons.png");
								moveLeft = new Clickable(new Vector2(0,0), new Vector2(scr_width/4, scr_width/4), scr_height);
								moveRight = new Clickable(new Vector2((3*scr_width)/4,0), new Vector2(scr_width/4, scr_width/4), scr_height);
							
								rocketButton = new StaticImg(new Vector2((4*scr_width)/10, 2), new Vector2(scr_width/5, scr_width/5), "buttons/rocket.png");
								shootRocket = new Clickable(new Vector2(scr_width/4, 0), new Vector2(scr_width/2, scr_width/4), scr_height);
							}
						}
						
						if(waters.isClicked()) {
							stateInit = false;
							gameState = 3; // Switch to playing on the streets map with the accelerometer
							
							if(!useAccelerometer) {
								ctlButtons = new StaticImg(new Vector2(0, scr_width/50), new Vector2(scr_width, scr_width/8), "buttons/ctlButtons.png");
								moveLeft = new Clickable(new Vector2(0,0), new Vector2(scr_width/4, scr_width/4), scr_height);
								moveRight = new Clickable(new Vector2((3*scr_width)/4,0), new Vector2(scr_width/4, scr_width/4), scr_height);
							
								rocketButton = new StaticImg(new Vector2((4*scr_width)/10, 2), new Vector2(scr_width/5, scr_width/5), "buttons/rocket.png");
								shootRocket = new Clickable(new Vector2(scr_width/4, 0), new Vector2(scr_width/2, scr_width/4), scr_height);
							}
						}
						
						if(settingsButton.isClicked()) {
							settingsBg.active = true;
							musicBtn.active = true;
							sfxBtn.active = true;
							hiResBtn.active = true;
							ctlBtn.active = true;
							menuLoadTimer = 0.5f;
							settingsMode = true;
						}
						
						if(htpBtn.isClicked()) {
							howToPlayBg.active = true;
							howToPlayMode = true;
							menuLoadTimer = 0.5f;
						}
						
						if(storeBtn.isClicked()) {
							storeBg.active = true;
							storeMode = true;
							menuLoadTimer = 0.5f;
						}
						
					} else {
						menuLoadTimer += Gdx.graphics.getDeltaTime();
					}
				}
				
			} else {
				
				// Create Title
				
				title = new DynamicImg(new Vector2(scr_width/12, scr_height + 20), new Vector2(scr_width/12, scr_height - (scr_width/2)), new Vector2((5*scr_width)/6, (5*scr_width)/12), "menu/title.png", 10, true);
				
				// Bird Animation
				
				if(settings.Data[2] == 1) {
					menuBird = new dynamicAnimation(new Vector2(scr_width + 40, scr_height - (11*scr_width)/12), new Vector2((scr_width)/8, scr_height - (11*scr_width)/12), new Vector2(scr_width, scr_width), "menu/timbo.png", 1.8f, 0.03125f, 20);
				} else {
					menuBird = new dynamicAnimation(new Vector2(scr_width + 40, scr_height - (11*scr_width)/12), new Vector2((scr_width)/8, scr_height - (11*scr_width)/12), new Vector2(scr_width, scr_width), "menu/timbo-lores.png", 1.8f, 0.03125f, 20);
				}
				
				// Create Scrolling Background Texture
				
				menuBg = new Texture(Gdx.files.internal("menu/menuSky.png"));
				
				menuBg.setWrap(TextureWrap.Repeat, TextureWrap.Repeat);
				
				menuBgSprite = new Sprite(menuBg);
				
				stateInit = true; // Set Initialized flag to true
				
				gameMenu = new DynamicImg(new Vector2(0,-scr_width - 20), new Vector2(0,0), new Vector2(scr_width, scr_width), "menu/buttons.png", 10, true);
				
				streets = new Clickable(new Vector2((scr_width*27)/1024, (scr_width*27)/1024), new Vector2((scr_width*396)/1024, (scr_width*787)/1024),scr_height);
				
				waters = new Clickable(new Vector2((scr_width*435)/1024, (scr_width*27)/1024), new Vector2((scr_width*396)/1024, (scr_width*787)/1024),scr_height);
				
				settingsBg = new DynamicImg(new Vector2((107*scr_width)/128, 0), new Vector2(0, 0), new Vector2(scr_width, scr_width), "menu/settings.png", 12, false);
				howToPlayBg = new DynamicImg(new Vector2((107*scr_width)/128, 0), new Vector2(0, 0), new Vector2(scr_width, scr_width), "menu/howtoplay.png", 12, false);
				storeBg = new DynamicImg2(new Vector2((107*scr_width)/128, 0), new Vector2(0, 0), new Vector2(scr_width, scr_width), "menu/store.png", "menu/store_green.png", 12, false);
				
				settingsButton = new Clickable(new Vector2((107*scr_width)/128, scr_width/16), new Vector2((21*scr_width)/128, (27*scr_width)/128), scr_height);
				settingsButton2 = new Clickable(new Vector2(0, scr_width/16), new Vector2((21*scr_width)/128, (27*scr_width)/128), scr_height);
				
				htpBtn = new Clickable(new Vector2((107*scr_width)/128, (147*scr_width)/512), new Vector2((21*scr_width)/128, (27*scr_width)/128), scr_height);
				htpBtn2 = new Clickable(new Vector2(0, (147*scr_width)/512), new Vector2((21*scr_width)/128, (27*scr_width)/128), scr_height);
				
				storeBtn = new Clickable(new Vector2((107*scr_width)/128, (17*scr_width)/32), new Vector2((21*scr_width)/128, (27*scr_width)/128), scr_height);
				storeBtn2 = new Clickable(new Vector2(0, (17*scr_width)/32), new Vector2((21*scr_width)/128, (27*scr_width)/128), scr_height);
				
				pack3 = new Clickable(new Vector2((73*scr_width)/128, (347*scr_width)/512), new Vector2((33*scr_width)/128, (73*scr_width)/512), scr_height);
				pack10 = new Clickable(new Vector2((73*scr_width)/128, (268*scr_width)/512), new Vector2((33*scr_width)/128, (73*scr_width)/512), scr_height);
				pack20 = new Clickable(new Vector2((73*scr_width)/128, (184*scr_width)/512), new Vector2((33*scr_width)/128, (73*scr_width)/512), scr_height);
				prun5 = new Clickable(new Vector2((73*scr_width)/128, (102*scr_width)/512), new Vector2((33*scr_width)/128, (73*scr_width)/512), scr_height);
				
				nest = new DispDigits(new Vector2((145*scr_width)/256,(23*scr_width)/512), new Vector2(scr_width/12, scr_width/10), "text/digits.png", "text/empty.png", true, true);

				if(settings.Data[0] == 1) {
					musicBtn = new DynamicCheckButton(new Vector2((scr_width*199)/128, (scr_width*185)/256), new Vector2((scr_width*75)/128, (scr_width*185)/256), new Vector2(scr_width/3, scr_width/12), "menu/on.png", "menu/off.png", 12, false, true, scr_height);
				} else {
					musicBtn = new DynamicCheckButton(new Vector2((scr_width*199)/128, (scr_width*185)/256), new Vector2((scr_width*75)/128, (scr_width*185)/256), new Vector2(scr_width/3, scr_width/12), "menu/on.png", "menu/off.png", 12, false, false, scr_height);
				}
				
				if(settings.Data[1] == 1) {
					sfxBtn = new DynamicCheckButton(new Vector2((scr_width*199)/128, (scr_width*75)/128), new Vector2((scr_width*75)/128, (scr_width*75)/128), new Vector2(scr_width/3, scr_width/12), "menu/on.png", "menu/off.png", 12, false, true, scr_height);
				} else {
					sfxBtn = new DynamicCheckButton(new Vector2((scr_width*199)/128, (scr_width*75)/128), new Vector2((scr_width*75)/128, (scr_width*75)/128), new Vector2(scr_width/3, scr_width/12), "menu/on.png", "menu/off.png", 12, false, false, scr_height);
				}
				
				if(settings.Data[2] == 1) {
					hiResBtn = new DynamicCheckButton(new Vector2((scr_width*199)/128, (scr_width*113)/256), new Vector2((scr_width*75)/128, (scr_width*113)/256), new Vector2(scr_width/3, scr_width/12), "menu/on.png", "menu/off.png", 12, false, true, scr_height);
				} else {
					hiResBtn = new DynamicCheckButton(new Vector2((scr_width*199)/128, (scr_width*113)/256), new Vector2((scr_width*75)/128, (scr_width*113)/256), new Vector2(scr_width/3, scr_width/12), "menu/on.png", "menu/off.png", 12, false, false, scr_height);
				}
				
				if(settings.Data[3] == 1) {
					ctlBtn = new DynamicCheckButton(new Vector2((scr_width*199)/128, (scr_width*39)/128), new Vector2((scr_width*75)/128, (scr_width*39)/128), new Vector2(scr_width/3, scr_width/12), "menu/tilt.png", "menu/touch.png", 12, false, true, scr_height);
				} else {
					ctlBtn = new DynamicCheckButton(new Vector2((scr_width*199)/128, (scr_width*39)/128), new Vector2((scr_width*75)/128, (scr_width*39)/128), new Vector2(scr_width/3, scr_width/12), "menu/tilt.png", "menu/touch.png", 12, false, false, scr_height);
				}
				
				accelSlider = new DynamicSlider(new Vector2((scr_width*639)/512, (scr_width*1)/16), new Vector2((scr_width*127)/512, (scr_width*1)/16), new Vector2((scr_width*87)/128, (scr_width*87)/512), "menu/accel-sensitivity.png", "menu/accel-slider.png", 12, false, scr_height, new Vector2((scr_width*63)/256, scr_width/16), new Vector2((scr_width*225)/256, scr_width/16), new Vector2(scr_width/16, scr_width/16));
				accelSlider.valueNorm = (float) settings.Data[4]/200;
				if(accelSlider.valueNorm > 1) {
					accelSlider.valueNorm = 1;
				} else if(accelSlider.valueNorm < 0) {
					accelSlider.valueNorm = 0;
				}
				
				theme.get(0).setVolume(musicVol);
				theme.get(0).play();
				
			}
			
		} else if((gameState == 2) || (gameState == 3)) { // PLAY MODE
			
			if(stateInit) {
				
				background.scroll(batch, scr_height, bgScrollRate, paused);
					
				if(paused) {
					
					startRun.draw(batch);
					
					for(Obstacle obs: obstacles) {
						if(obs.onScreen) {
							obs.render(batch);
						}
					}
				
					// Render Bird
					activeBird.render(batch);
					
					for(Explosion exp: explosions) {
						if(exp.onScreen) {
							exp.render(batch);
						}
					}
					
					if(!useAccelerometer) { // Draw buttons
						ctlButtons.render(batch);
						if(numRockets > 0) {
							rocketButton.render(batch);
						}
					}
					
					score.display(batch, checkpoints);
					
					nRockets.display(batch, numRockets + purchases.Data[0]);
					
					pauseMenu.active = true;
					
					if(resumeBtn.isClicked()) {
						paused = false;
						rocketLoadTimer = 0f;
						theme.get(1).play();
						theme.get(1).setLooping(true);
					}
					
					pauseMenu.render(batch);
					
					if(purchases.Data[1] == 1) {
						storeBg.render(batch, false);
					} else {
						storeBg.render(batch, true);
					}
					howToPlayBg.render(batch);
					settingsBg.render(batch);
					musicBtn.render(batch);
					sfxBtn.render(batch);
					hiResBtn.render(batch);
					ctlBtn.render(batch);
					accelSlider.render(batch);
					if((settings.Data[3] == 1) && (settingsBg.active)) {
						accelSlider.active = true;
					} else {
						accelSlider.active = false;
					}
					
					if(settingsMode) {
						
						if(settingsButton2.isClicked()) {
							settingsBg.active = false;
							musicBtn.active = false;
							sfxBtn.active = false;
							hiResBtn.active = false;
							ctlBtn.active = false;
							menuLoadTimer = 0.5f;
							settingsMode = false;
						}
						
						if(musicBtn.isClicked()) {
							musicBtn.checked = !musicBtn.checked;
							if(musicBtn.checked) {
								musicVol = 1;
							} else {
								musicVol = 0;
							}
							settings.Data[0] = (int) musicVol;
							theme.get(0).setVolume(musicVol);
							theme.get(1).setVolume(musicVol);
							try {
								LocalData.saveData(settings, "rocketbird/data/settings.dat");
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						if(sfxBtn.isClicked()) {
							sfxBtn.checked = !sfxBtn.checked;
							if(sfxBtn.checked) {
								sfxVol = 1;
							} else {
								sfxVol = 0;
							}
							settings.Data[1] = (int) sfxVol;
							try {
								LocalData.saveData(settings, "rocketbird/data/settings.dat");
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						if(hiResBtn.isClicked()) {
							hiResBtn.checked = !hiResBtn.checked;
							if(hiResBtn.checked) {
								settings.Data[2] = 1;
							} else {
								settings.Data[2] = 0;
							}
							try {
								LocalData.saveData(settings, "rocketbird/data/settings.dat");
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						if(ctlBtn.isClicked()) {
							ctlBtn.checked = !ctlBtn.checked;
							if(ctlBtn.checked) {
								settings.Data[3] = 1;
								useAccelerometer = true;
							} else {
								settings.Data[3] = 0;
								useAccelerometer = false;
							}
							try {
								LocalData.saveData(settings, "rocketbird/data/settings.dat");
							} catch (IOException e) {
								e.printStackTrace();
							}
						}
						if(settings.Data[3] == 1) {
							settings.Data[4] = (int) ((int) 200 * accelSlider.sliderValue());
						}
						
					} else if (howToPlayMode) {
						
						if(htpBtn2.isClicked()) {
							howToPlayBg.active = false;
							howToPlayMode = false;
						}
						
						if(settingsButton.isClicked()) {
							settingsBg.active = true;
							musicBtn.active = true;
							sfxBtn.active = true;
							hiResBtn.active = true;
							ctlBtn.active = true;
							menuLoadTimer = 0.5f;
							settingsMode = true;
							howToPlayBg.active = false;
							howToPlayMode = false;
							storeBg.active = false;
							storeMode = false;
						}
						
					} else if (storeMode) { 
						
						if(Math.abs(storeBg.position.x - storeBg.position2.x) < 20) {
							nest.display(batch, purchases.Data[0]);
						}
						
						if(storeBtn2.isClicked()) {
							storeMode = false;
							storeBg.active = false;
						}
						
						if(htpBtn.isClicked()) {
							howToPlayBg.active = true;
							howToPlayMode = true;
							menuLoadTimer = 0.5f;
							storeMode = false;
							storeBg.active = false;
						}
						
						if(settingsButton.isClicked()) {
							settingsBg.active = true;
							musicBtn.active = true;
							sfxBtn.active = true;
							hiResBtn.active = true;
							ctlBtn.active = true;
							menuLoadTimer = 0.5f;
							settingsMode = true;
							howToPlayBg.active = false;
							howToPlayMode = false;
						}
						
						if(Math.abs(storeBg.position.x - storeBg.position2.x) < 2) {
						
							if(pack3.isClicked()) {
								tryPurchase("rocket_pack_3");
							} else if(pack10.isClicked()) {
								tryPurchase("rocket_pack_10");
							} else if(pack20.isClicked()) {
								tryPurchase("rocket_pack_20");
							} else if(prun5.isClicked()) {
								if(purchases.Data[1] == 0) {
									tryPurchase("rockets_5_per_run");
								}
							}
							
						}
						
					} else {
						if(settingsButton.isClicked()) {
							settingsBg.active = true;
							musicBtn.active = true;
							sfxBtn.active = true;
							hiResBtn.active = true;
							ctlBtn.active = true;
							menuLoadTimer = 0.5f;
							settingsMode = true;
						}
						
						if(htpBtn.isClicked()) {
							howToPlayBg.active = true;
							howToPlayMode = true;
							menuLoadTimer = 0.5f;
						}
						
						if(storeBtn.isClicked()) {
							storeBg.active = true;
							storeMode = true;
							menuLoadTimer = 0.5f;
						}
					}
					
					if(homeBtn.isClicked()) {
						if(purchases.Data[1] == 1) {
							numRockets = 5; // Start with 5 rockets
						} else {
							numRockets = 3; // Start with 3 rockets
						}
						gameTimer = 0.0f;
						gameoverTimer = 0.0f;
						goMenu.reset();
						dist.reset();
						best.reset();
						gameOver.reset();
						rocketLoadTimer = -0.5f;
						collided = false; 
						bgScrollRate = 400;
						frameCounter = 0f;
						distance = 0;
						for(Obstacle obs: obstacles) {
							obs.onScreen = false;
						}
						checkpoints = 0;
						checkpoint.added = false;
						title.reset();
						gameMenu.reset();
						activeBird.burstTime = 0f;
						gameState = 1;
						menuBird.reset();
						menuLoadTimer = 0.0f;
						displayedScore = 0;
						dingPlayed = false;
						sounds.get(7).stop();
						tallyLoopStarted = false;
						theme.get(0).play();
						startRun.reset();
						theme.get(0).setVolume(musicVol);
						theme.get(1).setVolume(musicVol);
						themeStopped = false;
						paused = false;
						pauseMenu.reset();
					}
					
				} else {
					
					startRun.render(batch);
					
					pauseMenu.active = false;
					
					pauseMenu.render(batch);
					
					
				
					if(!collided) {
						
						if((!theme.get(0).isPlaying()) && (!themeStarted)) {
							theme.get(1).play();
							theme.get(1).setLooping(true);
							theme.get(1).setVolume(musicVol);
							themeStarted = true;
						}
						
						if(!theme.get(0).isPlaying() && !theme.get(1).isPlaying()) {
							theme.get(1).play();
							theme.get(1).setLooping(true);
							theme.get(1).setVolume(musicVol);
							themeStarted = true;
						}
						
						if(Gdx.graphics.getDeltaTime() < 0.1f) {
							gameTimer += Gdx.graphics.getDeltaTime();
							distance = (int) (gameTimer * 10);
							frameCounter += Gdx.graphics.getDeltaTime();
						}
						
						// Generate Obstacle every second
						
						if(gameState == 2) {
							if(frameCounter*50 >= entryRate) {
								
								frameCounter = 0f;
								
								// Create Obstacle
								
								// Number of Vehicles
								
								int numObstacles, obsCount = 0;
								
								if(obstacles.size() > 0) {
									for(Obstacle obstacle: obstacles) {
										if(obstacle.onScreen) {
											obsCount++;
										}
									}
								}
		
								if((obsCount < 4) || (distance > 250)) {
									if(distance > 500) {
										numObstacles = 2;
										speed = 12*(scr_height/960);
										entryRate = 40;
									} else if(distance > 250) {
										numObstacles = 2;
										speed = 11*(scr_height/960);
										entryRate = 40;
									} else if(distance > 150) {
										numObstacles = randInt(3);
									} else if(distance > 50) {
										entryRate = 45;
										numObstacles = randInt(2);
										speed = 10*(scr_height/960);
									} else {
										numObstacles = 0;
									}
									
									// Generate Vehicle
									int type1, type2;
									
									if(numObstacles == 1) {
										int pos = randInt(3);
										type1 = randInt(6);
										obstacles.add(new Obstacle(obsId, randInt(5), new Vector2(scr_width*(obsCenter[pos])/1024, scr_height), new Vector2((scr_width * 232)/1024, (scr_width * 232)/512), (int) (speed) + pos, cars.get(type1)));
										obsId += 1;
									} else if(numObstacles == 2) {
										int emptyPos = randInt(3);
										int pos1, pos2;
										if(emptyPos == 0) {
											pos1 = 1;
											pos2 = 2;
										} else if(emptyPos == 1) {
											pos1 = 0;
											pos2 = 2;
										} else {
											pos1 = 0;
											pos2 = 1;
										}
										if(distance > 600) {
											if(distance > 700) {
												speed = 15*(scr_height/960);
											}
											type1 = randInt(8);
											type2 = randInt(8);
											if(type1 == 7) {
												if(randInt(5) == 0) {
													type1 = 7;
												} else {
													if(randInt(4) == 0) {
														type1 = 6;
													} else {
														type1 = randInt(6);
													}
												}
											}
											type2 = randInt(7);
											if(type2 == 6) {
												if(randInt(5) == 0) {
													type2 = 7;
												} else {
													if(randInt(6) == 0) {
														type2 = 6;
													} else {
														type2 = randInt(6);
													}
												}
											}
										} else if(distance > 300) {
											type1 = randInt(8);
											if(type1 == 7) {
												if(randInt(10) == 0) {
													type1 = 7;
												} else {
													if(randInt(4) == 0) {
														type1 = 6;
													} else {
														type1 = randInt(6);
													}
												}
											}
											type2 = randInt(7);
											if(type2 == 6) {
												if(randInt(8) == 0) {
													type2 = 7;
												} else {
													if(randInt(6) == 0) {
														type2 = 6;
													} else {
														type2 = randInt(6);
													}
												}
											}
										} else {
											type1 = randInt(6);
											type2 = randInt(6);
										}
										if(type1 == 7) {
											collectables.add(new Collectable(new Vector2(scr_width*(obsCenter[pos1]-44)/1024, scr_height), new Vector2((scr_width * 232)/2048, (scr_width * 232)/2048), (int) speed - 2, collectRocket));
										} else if(type1 == 6) {
											obstacles.add(new Obstacle(obsId, 0, new Vector2(scr_width*(obsCenter[pos1])/1024, scr_height), new Vector2((scr_width * 232)/1024, (scr_width * 232)/300), (int) speed + pos1, cars.get(type1)));
										} else {
											obstacles.add(new Obstacle(obsId, 0, new Vector2(scr_width*(obsCenter[pos1])/1024, scr_height), new Vector2((scr_width * 232)/1024, (scr_width * 232)/512), (int) speed + pos1, cars.get(type1)));
										}
										obsId += 1;
										if(type2 == 7) {
											collectables.add(new Collectable(new Vector2(scr_width*(obsCenter[pos2]-44)/1024, scr_height), new Vector2((scr_width * 232)/2048, (scr_width * 232)/2048), (int) speed - 2, collectRocket));
										} else if(type2 == 6) {
											obstacles.add(new Obstacle(obsId, 0 , new Vector2(scr_width*(obsCenter[pos2])/1024, scr_height), new Vector2((scr_width * 232)/1024, (scr_width * 232)/300), (int) (speed + pos2), cars.get(type2)));
										} else {
											obstacles.add(new Obstacle(obsId, 0 , new Vector2(scr_width*(obsCenter[pos2])/1024, scr_height), new Vector2((scr_width * 232)/1024, (scr_width * 232)/512), (int) (speed + pos2), cars.get(type2)));
										}
										obsId += 1 ;
									}
									
								}
								
							}
						} else {
							if(frameCounter*40 > entryRate) {
								frameCounter = 0;
								
								int type1 = 99, type2 = 99;
								
								if(distance > 500) {
									speed = 13*(scr_height/960);
									
									if(distance > 750) {
										speed = 15*(scr_height/960);
									}
									
									int randNum = randInt(10);
									if(randNum == 9) {
										type1 = 5; // Aircraft carrier
										entryRate = (int) (((scr_width*8)/3)/speed);
									} else if(randNum == 8) {
										type1 = 6; // Collect Rocket
										entryRate = (int) ((scr_width/4)/speed);
									} else {
										type1 = randInt(5);
										if(type1 < 3) {
											entryRate = (int) ((int) ((scr_width*2)/3)/speed);
										} else {
											entryRate = (int) ((int) ((scr_width*4)/3)/speed);
										}
										type2 = randInt(3);
									}
									
								} else if(distance > 300) {
									speed = 11*(scr_height/960);
									
									int randNum = randInt(15);
									if(randNum == 14) {
										type1 = 5; // Aircraft carrier
										entryRate = (int) (((scr_width*8)/3)/speed);
									} else if(randNum == 13) {
										type1 = 6; // Collect Rocket
										entryRate = (int) ((scr_width/4)/speed);
									} else {
										type1 = randInt(5);
										if(type1 < 3) {
											entryRate = (int) ((int) ((scr_width*2)/3)/speed);
										} else {
											entryRate = (int) ((int) ((scr_width*4)/3)/speed);
										}
										type2 = randInt(3);
									}
									
								} else if(distance > 125) {
									speed = 10*(scr_height/960);
									
									type1 = randInt(5);
									if(type1 < 3) {
										entryRate = (int) ((int) ((scr_width*2)/3)/speed);
									} else {
										entryRate = (int) ((int) ((scr_width*4)/3)/speed);
									}
									type2 = randInt(3);
									
								} else if(distance > 50) {
									speed = 10*(scr_height/960);
									
									type1 = randInt(6);
								
									if(type1 < 3) {
										entryRate = (int) ((int) ((scr_width*2)/3)/speed);
									} else if(type1 < 5) {
										entryRate = (int) ((int) ((scr_width*4)/3)/speed);
									} else {
										type1 = 99;
									}
									
								} else {
									type1 = 99;
									type2 = 99;
								}
								
								if(type1 == 6) {
									collectables.add(new Collectable(new Vector2(randInt((scr_width*227)/256), scr_height), new Vector2((scr_width * 232)/2048, (scr_width * 232)/2048), (int) speed, collectRocket));
								} else if(type1 == 5) {
									tmpPos1 = (int) (randInt(2)*((scr_width*2)/3) + (scr_width/8));
									obstacles.add(new Obstacle(obsId, 2, new Vector2(tmpPos1, scr_height), new Vector2((scr_width * 2)/3, (scr_width * 8)/3), (int) speed , boats.get(type1)));
								} else if(type1 < 3) {
									tmpPos1 = (int) ((int) randInt((scr_width*3)/4) + scr_width/4);
									obstacles.add(new Obstacle(obsId, 0, new Vector2(tmpPos1, scr_height), new Vector2(scr_width/4, scr_width/2), (int) speed ,boats.get(type1)));
								} else if(type1 < 5){
									tmpPos1 = (int) ((int) randInt((scr_width*3)/4) + scr_width/4);
									obstacles.add(new Obstacle(obsId, 1, new Vector2(tmpPos1, scr_height), new Vector2(scr_width/4, scr_width), (int) speed ,boats.get(type1)));
								}
								obsId += 1;
								
								if(tmpPos1 > (6*scr_width)/10) {
									tmpPos2 = (int) ((int) tmpPos1 - ((scr_width*9)/16));
								} else {
									tmpPos2 = (int) (tmpPos1 + ((scr_width*9)/16));
								}
								
								if(type2 < 3) {
									obstacles.add(new Obstacle(obsId, 0, new Vector2(tmpPos2, scr_height + scr_width/8), new Vector2(scr_width/4, scr_width/2), (int) speed ,boats.get(type2)));
								} else if(type2 < 5) {
									obstacles.add(new Obstacle(obsId, 1, new Vector2(tmpPos2, scr_height + scr_width/8), new Vector2(scr_width/4, scr_width/2), (int) speed , boats.get(type2)));
								}
								obsId += 1;
								
							}
						}
						
						for(Collectable col: collectables) {
							if(col.onScreen) {
								col.update();
								col.render(batch);
								
								int colLeft = (int) (col.position.x - activeBird.size.x);
								int colRight = (int) (col.position.x + col.size.x);
								int colTop = (int) (col.position.y + col.size.y);
								int colBottom = (int) (col.position.y - (activeBird.size.y/2));
								
								if((activeBird.position.x > colLeft) && (activeBird.position.x < colRight) && (activeBird.position.y > colBottom) && (activeBird.position.y < colTop)) {
									col.onScreen = false;
									numRockets += 1;
									sounds.get(3).play(sfxVol);
								}
								
								
							}
						}
						
						// Update and Render Obstacles
						if(obstacles.size() > 0) {
							for(Obstacle obstacle: obstacles) {
								if(obstacle.onScreen) {
									
									// OBSTACLE AI
									for(Obstacle obs2: obstacles) {
										if(obs2.onScreen && obs2.id != obstacle.id) {
											if((obs2.position.x == obstacle.position.x) && (obs2.position.y < obstacle.position.y) && (obs2.position.y + (2*obs2.size.y) >= obstacle.position.y)) {
												obstacle.speed = obs2.speed;
												obstacle.position.y = obs2.position.y + 2*(obs2.size.y);
											}
										}
									}
									
									obstacle.update();
									obstacle.render(batch);
									
									// Check for collision
									
									// Collision Region Bounds
									float collRegionLeft = obstacle.position.x - (obstacle.size.x/2) - (7*activeBird.size.x)/8;
									float collRegionRight = obstacle.position.x + (obstacle.size.x/2) - (activeBird.size.x/8);
									float collRegionTop = obstacle.position.y + ((7*obstacle.size.y)/8) - (activeBird.size.y/8);
									float collRegionBottom = obstacle.position.y + (obstacle.size.y/8) - (7*activeBird.size.y)/8;
									
									// Check if sprite is in the collision region
									if((activeBird.position.x > collRegionLeft) && (activeBird.position.x < collRegionRight) && (activeBird.position.y > collRegionBottom) && (activeBird.position.y < collRegionTop)){
										collided = true;									
										try {
											data = LocalData.readData("rocketbird/data/local.dat");
										} catch (ClassNotFoundException e) {
											e.printStackTrace();
										} catch (IOException e) {
											e.printStackTrace();
										}
										if(gameState == 2) {
											if(data.Data.length > 0) {
												if(checkpoints > data.Data[0]) {
													sounds.get(5).play(sfxVol);
												} else {
													sounds.get(1).play(sfxVol);
												}
											} else {
												data.Data[0] = 0;
												sounds.get(5).play(sfxVol);
											}
											
											if(checkpoints > data.Data[0]) {
												data.Data[0] = checkpoints;
												try {
													LocalData.saveData(data, "rocketbird/data/local.dat");
												} catch (IOException e) {
													e.printStackTrace();
												}
												
											}
										} else {
											if(data.Data.length > 0) {
												if(checkpoints > data.Data[1]) {
													sounds.get(5).play(sfxVol);
												} else {
													sounds.get(1).play(sfxVol);
												}
											} else {
												data.Data[1] = 0;
												sounds.get(5).play(sfxVol);
											}
											
											if(checkpoints > data.Data[1]) {
												data.Data[1] = checkpoints;
												try {
													LocalData.saveData(data, "rocketbird/data/local.dat");
												} catch (IOException e) {
													e.printStackTrace();
												}
											}
										}
									}
								}
							}
						}
						
						if(useAccelerometer) {
						
							// Read Accelerometer input
							float tiltX = Gdx.input.getAccelerometerX();
							
							// Limit Sprite to screen width
							if(((tiltX > 0) && (activeBird.position.x > 0)) || ((tiltX < 0) && (activeBird.position.x < scr_width - bird_size.x))) {
								activeBird.position.x -= ((tiltX*(accelSlider.valueNorm + 0.5))/10) * (scr_width/20);
								activeBird.heading = 8*tiltX;
							} else {
								activeBird.heading = approachExpo(activeBird.heading, 0, 3);
							}
						
						} else {
							
							// Input from Control Buttons
							if((activeBird.position.x > 0) && ((moveLeft.isClicked() || Gdx.input.isKeyPressed(Keys.LEFT)))) {
								turnSpd = (int) approachExpo(turnSpd, 15, 4);
								activeBird.position.x -= turnSpd;
								activeBird.heading = approachExpo(activeBird.heading, 30, 4);
							} else if((moveRight.isClicked() || Gdx.input.isKeyPressed(Keys.RIGHT))  && (activeBird.position.x < scr_width - bird_size.x)) {
								turnSpd = (int) approachExpo(turnSpd, 15, 4);
								activeBird.position.x += turnSpd;
								activeBird.heading = approachExpo(activeBird.heading, -30, 4);
							} else {
								activeBird.heading = approachExpo(activeBird.heading, 0, 3);
								turnSpd = 0;
							}
							
							if(Gdx.input.isKeyPressed(Keys.P)) {
								paused = true;
								if(theme.get(1).isPlaying()) {
									theme.get(1).pause();
								} else if(theme.get(0).isPlaying()) {
									theme.get(0).pause();
								}
							}
							
							// Keyboard Controls for desktop version
							/* if(Gdx.input.isKeyPressed(Keys.LEFT) && activeBird.position.x > 0) {
								turnSpd = (int) approachExpo(turnSpd, 15, 4);
								activeBird.position.x -= turnSpd;
								activeBird.heading = approachExpo(activeBird.heading, 30, 4);
							} else if(Gdx.input.isKeyPressed(Keys.RIGHT) && activeBird.position.x < scr_width - bird_size.x) {
								turnSpd = (int) approachExpo(turnSpd, 15, 4);
								activeBird.position.x += turnSpd;
								activeBird.heading = approachExpo(activeBird.heading, -30, 4);
							} else {
								activeBird.heading = approachExpo(activeBird.heading, 0, 3);
								turnSpd = 0;
							} */
							
						}
						
						// Shoot Rocket
						if(useAccelerometer) {
							if((numRockets + purchases.Data[0] > 0) && (rocketLoadTimer > 0.25) && (Gdx.input.isTouched())) {
								rockets.add(new Projectile(new Vector2(activeBird.position.x + (activeBird.size.x/3), activeBird.position.y + (activeBird.position.y/2)), new Vector2(activeBird.size.x/3, (activeBird.size.y*4)/3), rocketTexture, (int) (8*(scr_height/960)), 4, 1, 0.0625f, 0.25f, sounds.get(0).play(sfxVol), sfxVol));
								rocketLoadTimer = 0;
								if(numRockets > 0) {
									numRockets -= 1;
								} else {
									purchases.Data[0] -= 1;
									try {
										LocalData.saveData(purchases, "rocketbird/data/purchases.dat");
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									
								}
							}
						} else {
							if((rocketLoadTimer > 0.25) && (numRockets + purchases.Data[0] > 0) && ((shootRocket.isClicked()) || (Gdx.input.isKeyPressed(Keys.SPACE)))) {
								rockets.add(new Projectile(new Vector2(activeBird.position.x + (activeBird.size.x/3), activeBird.position.y + (activeBird.position.y/2)), new Vector2(activeBird.size.x/3, (activeBird.size.y*4)/3), rocketTexture, (int) (8*(scr_height/960)), 4, 1, 0.0625f, 0.25f, sounds.get(0).play(sfxVol), sfxVol));
								rocketLoadTimer = 0;
								if(numRockets > 0) {
									numRockets -= 1;
								} else {
									purchases.Data[0] -= 1;
									try {
										LocalData.saveData(purchases, "rocketbird/data/purchases.dat");
									} catch (IOException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
									
								}
							}
						}
						
						// Load next rocket
						if(rocketLoadTimer <= 0.25) {
							rocketLoadTimer += Gdx.graphics.getDeltaTime();
						}
						
						for (Projectile rocket: rockets) {
							
							if(rocket.onScreen) {
								
								if(rocket.update(obstacles, scr_height, sounds)) {
									explosions.add(new Explosion(new Vector2(rocket.position.x - (rocket.size.x/2) - (scr_width/4), rocket.position.y), new Vector2((2*scr_width)/3, (2*scr_width)/3), explosionTexture));
								}
								
								rocket.render(batch);
							} else if(rocket.volume > 0) {
								rocket.fadeOut(sounds);
							}
							
						}
						
						if(distance > 50) {
							if(checkpoint.pos.y <= scr_height/6) {
								checkpoint.render(batch, 0, scr_height);
								if(!checkpoint.added) {
									checkpoints += 1;
									checkpoint.added = true;
									sounds.get(9).play();
								}
							} else {
								checkpoint.render(batch, 1, scr_height);
							}
						}
						
						// Update Bird
						activeBird.update();
					
						// Render Bird
						activeBird.render(batch);
						
						for(Explosion exp: explosions) {
							if(exp.onScreen) {
								exp.update();
								exp.render(batch);
							}
						}
						
						if(!useAccelerometer) { // Draw buttons
							ctlButtons.render(batch);
							if(numRockets > 0) {
								rocketButton.render(batch);
							}
						}
						
						score.display(batch, checkpoints);
						
						nRockets.display(batch, numRockets + purchases.Data[0]);
						
						if(checkpoint.pos.y <= -checkpoint.size.y) {
							checkpoint.pos.y = scr_height;
							checkpoint.added = false;
						}
						
					} else {
						
						// Collision Sequence
						
						if(bgScrollRate > 0) {
							bgScrollRate -= 20;
						}
						
						for(Collectable col: collectables) {
							col.onScreen = false;
						}
						
						for(Projectile rocket: rockets) {
							rocket.onScreen = false;
						}
						
						gameoverTimer += Gdx.graphics.getDeltaTime();
						
						// Update and Render Obstacles
						if(obstacles.size() > 0) {
							for(Obstacle obstacle: obstacles) {
								if(obstacle.onScreen) {
									
									obstacle.speed = approachExpo(obstacle.speed, 0, 10);
									
									obstacle.update();
									obstacle.render(batch);
									
								}
							}
						}
						
						// Game over sequence
						
						activeBird.burstIntoConfetti(batch);
						
						if(gameoverTimer > 1.3) {
							if(!tallyLoopStarted) {
								sounds.get(7).loop(sfxVol * 0.8f);
								tallyLoopStarted = true;
							}						
							gameOver.render(batch);
							goMenu.render(batch);
							dist.display(batch, displayedScore);
							double upRate = 1;
							
							if(scoreUpCounter > 2) {
								if(displayedScore < checkpoints - upRate) {
									displayedScore += upRate;
								} else {
									displayedScore = checkpoints;
									sounds.get(7).stop();
									if(!dingPlayed) {
										sounds.get(8).play(sfxVol);
										dingPlayed = true;
									}
								}
								scoreUpCounter = 0;
							} else {
								scoreUpCounter += 1;
							}
							
							if(gameState == 2) {
								best.display(batch, data.Data[0]);
							} else {
								best.display(batch, data.Data[1]);
							}
							goSoundPlayed = false;
						} else if(gameoverTimer > 1) {
							gameOver.render(batch);
							if(!goSoundPlayed) {
								sounds.get(4).play(sfxVol);
								goSoundPlayed = true;
							}
						} else {
							theme.get(0).setLooping(false);
							theme.get(1).setLooping(false);
							theme.get(0).stop();
							theme.get(1).stop();
						}
						
						if(playOn.isClicked()) {
							tryPurchase("keep_playing");
						} else if(replay.isClicked()) {
							if(purchases.Data[1] == 1) {
								numRockets = 5; // Start with 5 rockets
							} else {
								numRockets = 3; // Start with 3 rockets
							}
							checkpoints = 0;
							checkpoint.added = false;
							gameTimer = 0.0f;
							gameoverTimer = 0.0f;
							goMenu.reset();
							dist.reset();
							best.reset();
							gameOver.reset();
							rocketLoadTimer = -0.5f;
							collided = false; 
							bgScrollRate = 400;
							frameCounter = 0f;
							distance = 0;
							for(Obstacle obs: obstacles) {
								obs.onScreen = false;
							}
							title.reset();
							gameMenu.reset();
							menuBird.reset();
							activeBird.burstTime = 0f;
							displayedScore = 0;
							dingPlayed = false;
							sounds.get(7).stop();
							tallyLoopStarted = false;
							theme.get(0).play();
							startRun.reset();
							theme.get(0).setVolume(musicVol);
							theme.get(1).setVolume(musicVol);
							themeStopped = false;
							checkpoints = 0;
							checkpoint.pos.y = scr_height;
							
						} else if(home.isClicked()) {
							if(purchases.Data[1] == 1) {
								numRockets = 5; // Start with 5 rockets
							} else {
								numRockets = 3; // Start with 3 rockets
							}
							checkpoints = 0;
							checkpoint.added = false;
							gameTimer = 0.0f;
							gameoverTimer = 0.0f;
							goMenu.reset();
							dist.reset();
							best.reset();
							gameOver.reset();
							rocketLoadTimer = -0.5f;
							collided = false; 
							bgScrollRate = 400;
							frameCounter = 0f;
							distance = 0;
							for(Obstacle obs: obstacles) {
								obs.onScreen = false;
							}
							title.reset();
							gameMenu.reset();
							activeBird.burstTime = 0f;
							gameState = 1;
							menuBird.reset();
							menuLoadTimer = 0.0f;
							displayedScore = 0;
							dingPlayed = false;
							sounds.get(7).stop();
							tallyLoopStarted = false;
							theme.get(0).play();
							startRun.reset();
							theme.get(0).setVolume(musicVol);
							theme.get(1).setVolume(musicVol);
							themeStopped = false;
							checkpoints = 0;
							checkpoint.pos.y = scr_height;
						}
						
						score.display(batch, checkpoints);
						
						nRockets.display(batch, numRockets + purchases.Data[0]);
						
					}
					
				}
				
			} else {
				
				// Create Active Bird Sprite
				
				bird_size = new Vector2(scr_width/4, scr_width/6);
				
				if(useAccelerometer) {
				
					activeBird = new Bird(new Vector2((scr_width/2) - (bird_size.x/2), scr_height/12), bird_size, "char/timbo.png");

				} else {
					
					activeBird = new Bird(new Vector2((scr_width/2) - (bird_size.x/2), scr_height/9), bird_size, "char/timbo.png");
					
				}
					
				if(gameState == 2) {
					
					background = new Scroller(new Vector2(0,0), new Vector2(scr_width, scr_width*2), "bg/road.png");
					
					
					
				} else {
					
					background = new Scroller(new Vector2(0,0), new Vector2(scr_width, scr_width*2), "bg/waters.png");
					
				}
				
				score = new DispDigits(new Vector2(12, scr_height - (scr_width/10) - 12), new Vector2(scr_width/12, scr_width/10), "text/digits.png", "text/ft.png", true, true);
				
				nRockets = new DispDigits(new Vector2(scr_width - 12, scr_height - (scr_width/10) - 12), new Vector2(scr_width/12, scr_width/10), "text/digits.png", "text/rocket-count.png", false, false);
				
				stateInit = true; // Set State Initialized Flag to true
				
				if(purchases.Data[1] == 1) {
					numRockets = 5; // Start with 5 rockets
				} else {
					numRockets = 3; // Start with 3 rockets
				}
				
				gameOver = new DynamicImg(new Vector2(scr_width/10, scr_height + 10), new Vector2(scr_width/10, scr_width), new Vector2((4*scr_width)/5, scr_width/5), "text/gameover.png", 8, true);
				
				gameoverTimer = 0.0f;
				
				goMenu = new DynamicImg(new Vector2(scr_width/10, -10 - (4*scr_width)/5), new Vector2(scr_width/10, scr_width/10), new Vector2((4*scr_width)/5, (4*scr_width)/5), "text/goMenu.png", 8, true);
				
				dist = new DynDispDigits(new Vector2((scr_width/10)+(51*scr_width)/128, -10 - scr_width/12), new Vector2((scr_width/10)+(51*scr_width)/128, (scr_width/10) + (196*scr_width)/320), new Vector2(scr_width/15,scr_width/12), "text/digits.png", "text/ft.png", true, 8);
				
				best = new DynDispDigits(new Vector2((scr_width/10)+(51*scr_width)/128, - 10 - scr_width/12), new Vector2((scr_width/10)+(51*scr_width)/128, (scr_width/10) + (320*scr_width)/640), new Vector2(scr_width/15,scr_width/12), "text/digits.png", "text/ft.png", true, 8);
				
				replay = new Clickable(new Vector2((99*scr_width)/541,(183*scr_width)/541), new Vector2((112*scr_width)/541,(112*scr_width)/541), scr_height);
				
				home = new Clickable(new Vector2((215*scr_width)/541,(183*scr_width)/541), new Vector2((112*scr_width)/541,(112*scr_width)/541), scr_height);

				goSoundPlayed = false;
				
				if(gameState == 2) {
					startRun = new DynamicImg(new Vector2(0, scr_height), new Vector2(0, -scr_height), new Vector2(scr_width, scr_width*4), "menu/321go.png", (int) (50 + ((scr_height*10)/960)), true);
				} else {
					startRun = new DynamicImg(new Vector2(0, scr_height), new Vector2(0, -scr_height), new Vector2(scr_width, scr_width*4), "menu/321goWaters.png", (int) (50 + ((scr_height*10)/960)), true);
				}
				
				pauseMenu = new DynamicImg(new Vector2(scr_width, 0), new Vector2(0, 0), new Vector2(scr_width, scr_width*2), "menu/pause.png", 8, false);
				
				resumeBtn = new Clickable(new Vector2((222*scr_width)/1024,scr_width), new Vector2((3*scr_width)/10,(3*scr_width)/10), scr_height);
				
				homeBtn = new Clickable(new Vector2((516*scr_width)/1024,scr_width), new Vector2((3*scr_width)/10,(3*scr_width)/10), scr_height);
				
				playOn = new Clickable(new Vector2((25*scr_width)/256,(35*scr_width)/512), new Vector2((103*scr_width)/128,(59*scr_width)/256), scr_height);
			
				checkpoint = new Checkpoint(new Vector2(0, scr_height), new Vector2(scr_width/7, scr_width/7), "bg/flag"+(gameState-2)+".png");
			}
			
		}
			
		batch.end();
	}
	
	@Override
	public void resize(int width, int height) {
	}

	@Override
	public void pause() {
	}

	@Override
	public void resume() {
	}
	
	// Approach target value smoothly
	public float approachExpo(float value, float target, float rate) {
		if(target > value) {
			return value + (target-value)/rate;
		} else if(target < value) {
			return value - (value-target)/rate;
		} else {
			return value;
		}
	}
	
	public int randInt(float f) {
		return (int) (Math.random() * f);
	}

	@Override
	public boolean keyDown(int keycode) {
		
		if(keycode == Keys.BACK) {
			if((gameState == 2) || (gameState == 3)) {
				if(paused) {
					paused = false;
					rocketLoadTimer = 0f;
					theme.get(1).play();
					theme.get(1).setLooping(true);
				} else {
					// Open Pause Menu
					paused = true;
					if(theme.get(1).isPlaying()) {
						theme.get(1).pause();
					} else if(theme.get(0).isPlaying()) {
						theme.get(0).pause();
					}
				}
			} else {
				Gdx.app.exit();
			}
		}
		
		return false;
	}

	@Override
	public boolean keyUp(int keycode) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean keyTyped(char character) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDown(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchUp(int screenX, int screenY, int pointer, int button) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean touchDragged(int screenX, int screenY, int pointer) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean mouseMoved(int screenX, int screenY) {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public boolean scrolled(int amount) {
		// TODO Auto-generated method stub
		return false;
	}
	
	public void tryPurchase(String item) {
		try {
			mHelper.launchPurchaseFlow(activity, item, 10001, mPurchaseFinishedListener);
		} catch(IllegalStateException ex) {
			activity.runOnUiThread(new Runnable() {
				public void run() {
					if(!toastCreated) {
						Toast.makeText(activity, "Please restart Rocket Birds to continue...", Toast.LENGTH_SHORT).show();
						toastCreated = true;
						Handler handler = new Handler();
						handler.postDelayed(new Runnable() {
							@Override
							public void run() {
								toastCreated = false;
							}
						}, 2000);
					}
				}
			});
		}
	}
}
