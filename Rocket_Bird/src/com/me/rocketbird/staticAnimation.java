package com.me.rocketbird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class staticAnimation {

	Vector2 position, size;
	String textureLoc;
	
	private static final int animCol = 8;
	private static final int animRow = 8;
	
	Animation animation;
	Texture texture;
	TextureRegion[] frames;
	TextureRegion currentFrame;
	float stateTime;
	float animLen;
	float animRate;
	
public staticAnimation(Vector2 position, Vector2 size, String textureLoc, float animLen, float animRate){
		
		this.position = position;
		this.size = size;
		texture = new Texture(Gdx.files.internal(textureLoc));
		TextureRegion[][] tmp = TextureRegion.split(texture, texture.getWidth()/animCol, texture.getHeight()/animRow);
		frames = new TextureRegion[animCol * animRow];
		
		this.animLen = animLen;
		this.animRate = animRate;
		
		int index = 0;
		
		for(int i = 0; i < animRow; i++) {
			for (int j = 0; j < animCol; j++) {
				frames[index++] = tmp[i][j];
			}
		}
		
		animation = new Animation(animRate, frames);
		stateTime = 0f;
		currentFrame = animation.getKeyFrame(0);
		
	}
	
	public void update() {
		
		if(stateTime < animLen) {
			stateTime += Gdx.graphics.getDeltaTime();
		} else {
			stateTime = 0;
		}
		currentFrame = animation.getKeyFrame(0 + stateTime);
	}
	
	public void render(SpriteBatch batch) {
		batch.draw(currentFrame, position.x, position.y, size.x, size.y);
	}
	
}
