package com.me.rocketbird;

import java.util.ArrayList;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;

public class Obstacle {
	
	Vector2 position, size;
	Texture obsTexture;
	int type;
	
	int id;
	float speed;
	boolean onScreen = true;
	
	public Obstacle(int id, int type, Vector2 position, Vector2 size, int speed, Texture texture) {
		this.position = position;
		this.size = size;
		this.type = type;
		obsTexture = texture;
		this.speed = speed;
	}
	
	public void update() {
		
		// Move down
		position.y -= speed;
		
		// Set to inactive when it goes below the screen
		if(position.y < -size.y) {
			onScreen = false;
		}
	}
	
	public void render(SpriteBatch batch) {
		batch.draw(obsTexture, position.x - size.x/2, position.y, size.x, size.y);
	}

}
