package com.me.rocketbird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class Bird {

	Vector2 position, size;
	String textureLoc;
	float heading = 0;
	
	private static final int animCol = 8;
	private static final int animRow = 8;
	
	Animation birdAnimate;
	Texture birdTexture;
	TextureRegion[] frames;
	TextureRegion currentFrame;
	float stateTime;
	
	Animation confettiAnimate;
	Texture confetti;
	Texture confettiLast;
	TextureRegion[] confettiFrames;
	TextureRegion confettiFrame;
	float burstTime = 0;
	
	public Bird(Vector2 position, Vector2 size, String textureLoc){
		
		this.position = position;
		this.size = size;
		birdTexture = new Texture(Gdx.files.internal(textureLoc));
		TextureRegion[][] tmp = TextureRegion.split(birdTexture, birdTexture.getWidth()/animCol, birdTexture.getHeight()/animRow);
		frames = new TextureRegion[animCol * animRow];
		
		int index = 0;
		
		for(int i = 0; i < animRow; i++) {
			for (int j = 0; j < animCol; j++) {
				frames[index++] = tmp[i][j];
			}
		}
		
		birdAnimate = new Animation(0.03125f, frames);
		stateTime = 0f;
		currentFrame = birdAnimate.getKeyFrame(0);
		
		confetti = new Texture(Gdx.files.internal("confetti/sequence.png"));
		confettiLast = new Texture(Gdx.files.internal("confetti/last.png"));
		tmp = TextureRegion.split(confetti, confetti.getWidth()/4, confetti.getHeight()/2);
		confettiFrames = new TextureRegion[8];
		
		index = 0;
		
		for(int i = 0; i < 2; i++) {
			for (int j = 0; j < 4; j++) {
				confettiFrames[index++] = tmp[i][j];
			}
		}
		
		confettiAnimate = new Animation(0.03125f, confettiFrames);
		
	}
	
	public void update() {
		
		if(stateTime < 2) {
			stateTime += Gdx.graphics.getDeltaTime();
		} else {
			stateTime = 0;
		}
		currentFrame = birdAnimate.getKeyFrame(0 + stateTime);
	}
	
	public void render(SpriteBatch batch) {
		batch.draw(currentFrame, position.x, position.y, size.x/2, size.y/2, size.x, size.y, 1, 1, heading);
	}
	
	public void burstIntoConfetti(SpriteBatch batch) {
		if(burstTime == 0) {
			confettiFrame = confettiAnimate.getKeyFrame(0);
		}
		
		if(burstTime < 0.25) {
			burstTime += Gdx.graphics.getDeltaTime();
			confettiFrame = confettiAnimate.getKeyFrame(0 + burstTime);
			batch.draw(confettiFrame, position.x - (size.x/4), position.y - (size.y/4), (size.x * 3)/2, (size.y * 3)/2);
		} else {
			batch.draw(confettiLast, position.x - (size.x/4), position.y - (size.y/4), (size.x * 3)/2, (size.y * 3)/2);
		}
	}
	
}
