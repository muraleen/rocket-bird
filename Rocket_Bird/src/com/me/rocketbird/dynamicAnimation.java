package com.me.rocketbird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class dynamicAnimation {

	Vector2 position1, position2, size, position;
	int p1_x, p1_y;
	String textureLoc;
	
	private static final int animCol = 8;
	private static final int animRow = 8;
	
	Animation animation;
	Texture texture;
	TextureRegion[] frames;
	TextureRegion currentFrame;
	float stateTime;
	float animLen;
	float animRate;
	int rate;
	
public dynamicAnimation(Vector2 position1, Vector2 position2, Vector2 size, String textureLoc, float animLen, float animRate, int rate){
		
		this.position1 = position1;
		this.position2 = position2;
		this.position = position1;
		p1_x = (int) position1.x;
		p1_y = (int) position1.y;
		this.size = size;
		texture = new Texture(Gdx.files.internal(textureLoc));
		TextureRegion[][] tmp = TextureRegion.split(texture, texture.getWidth()/animCol, texture.getHeight()/animRow);
		frames = new TextureRegion[animCol * animRow];
		
		this.rate = rate;
		this.animLen = animLen;
		this.animRate = animRate;
		
		int index = 0;
		
		for(int i = 0; i < animRow; i++) {
			for (int j = 0; j < animCol; j++) {
				frames[index++] = tmp[i][j];
			}
		}
		
		animation = new Animation(animRate, frames);
		stateTime = 0f;
		currentFrame = animation.getKeyFrame(0);
		
	}
	
	public void update() {
		
		if(stateTime < animLen) {
			stateTime += Gdx.graphics.getDeltaTime();
		} else {
			stateTime = 0;
		}
		currentFrame = animation.getKeyFrame(0 + stateTime);
	}
	
	public void render(SpriteBatch batch) {
		position.x = approachExpo(position.x,position2.x,rate);
		position.y = approachExpo(position.y,position2.y,rate);
		batch.draw(currentFrame, position.x, position.y, size.x, size.y);
	}
	
	public void reset() {
		this.position.x = p1_x;
		this.position.y = p1_y;
	}

	// Approach target value smoothly
	public float approachExpo(float value, float target, float rate) {
		if(target > value) {
			return value + (target-value)/rate;
		} else if(target < value) {
			return value - (value-target)/rate;
		} else {
			return value;
		}
	}
	
}

