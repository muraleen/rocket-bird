package com.me.rocketbird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class Collectable {
	
	boolean onScreen = true;
	
	Vector2 position, size;
	Animation animation;
	Texture texture;
	TextureRegion[] frames;
	TextureRegion currentFrame;
	float stateTime;
	float speed;
	
	public Collectable(Vector2 position, Vector2 size, int speed, Texture texture) {
		this.position = position;
		this.size = size;
		this.texture = texture;
		this.speed = speed;
		
		TextureRegion[][] tmp = TextureRegion.split(texture, texture.getWidth()/4, texture.getHeight()/4);
		frames = new TextureRegion[16];
		
		int index = 0;
		
		for(int i = 0; i < 4; i++) {
			for (int j = 0; j < 4; j++) {
				frames[index++] = tmp[i][j];
			}
		}
		
		animation = new Animation(0.05f, frames);
		stateTime = 0f;
		currentFrame = animation.getKeyFrame(0);
	}
	
	public void update() {
		if(stateTime < 0.8) {
			stateTime += Gdx.graphics.getDeltaTime();
		} else {
			stateTime = 0;
		}
		currentFrame = animation.getKeyFrame(0 + stateTime);
		position.y -= speed;
		if(position.y < -50) {
			this.onScreen = false;
		}
	}
	
	public void render(SpriteBatch batch) {
		batch.draw(currentFrame, position.x, position.y, size.x, size.y);
	}

}
