package com.me.rocketbird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;

public class Clickable {
	
	Vector2 position, size;
	int height;
	
	public Clickable(Vector2 position, Vector2 size, float scr_height) {
		this.position = position;
		this.size = size;
		this.height = (int) scr_height;
	}
	
	public boolean isClicked() {
		if(Gdx.input.isTouched()) {
			int touchX = Gdx.input.getX();
			int touchY = height - Gdx.input.getY();
			if((touchX > position.x) && (touchX < position.x + size.x) && (touchY > position.y) && (touchY < position.y + size.y)) {
				return true;
			} else {
				return false;
			}
		} else {
			return false;
		}
	}

}
