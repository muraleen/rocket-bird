package com.me.rocketbird;

import java.util.ArrayList;
import java.util.Iterator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class Projectile {
	
	Vector2 position, size;
	
	long sID;
	float volume, sfxVol;
	int animCol;
	int animRow;
	float repeat;
	int speed;
	Animation animation;
	TextureRegion[] frames;
	TextureRegion currentFrame;
	float stateTime;
	Texture texture;
	
	boolean onScreen;
	
	public Projectile(Vector2 position, Vector2 size, Texture texture, int speed, int col, int row, float rate, float repeat, long sID, float volume) {
		this.sID = sID;
		this.position = position;
		this.size = size;
		this.animCol = col;
		this.animRow = row;
		this.repeat = repeat;
		this.speed = speed;
		this.texture = texture;
		TextureRegion[][] tmp = TextureRegion.split(texture, texture.getWidth()/animCol, texture.getHeight()/animRow);
		frames = new TextureRegion[animCol * animRow];
		this.onScreen = true;
		this.sfxVol = volume;
		this.volume = volume;
		
		int index = 0;
		
		for(int i = 0; i < animRow; i++) {
			for (int j = 0; j < animCol; j++) {
				frames[index++] = tmp[i][j];
			}
		}
		
		animation = new Animation(rate, frames);
		stateTime = 0f;
		currentFrame = animation.getKeyFrame(0);
	}
	
	public boolean update(ArrayList<Obstacle> obstacles, float scr_height, ArrayList<Sound> sounds) {
		
		boolean hit = false;
		int explSize = 0;
		
		for (Iterator<Obstacle> iterator = obstacles.iterator(); iterator
				.hasNext();) {
			
			Obstacle obs = iterator.next();
			
			if(obs.onScreen) {
			
				int offLeft = (int) (obs.position.x - (obs.size.x/2) - (3*size.x)/4);
				int offRight = (int) (obs.position.x + (obs.size.x/2) - (size.x)/4);
				int offTop = (int) (obs.position.y + obs.size.y - (size.y/6));
				int offBottom = (int) (obs.position.y - (5*size.y)/6);
				
				if((position.x >= offLeft) && (position.x <= offRight) && (position.y >= offBottom) && (position.y <= offTop)) {
					hit = true;
					obs.onScreen = false;
					onScreen = false;
					sounds.get(2).play(sfxVol);
					explSize = obs.type;
				}
				
			}
			
		}
		
		if(!hit) {
			position.y += speed;
			
			if(position.y > scr_height) {
				onScreen = false;
			}
		}
		
		// Animate
		
		if(stateTime < repeat) {
			stateTime += Gdx.graphics.getDeltaTime();
		} else {
			stateTime = 0;
		}
		currentFrame = animation.getKeyFrame(0 + stateTime);
		
		return hit;
	}
	
	public void render(SpriteBatch batch) {
		batch.draw(currentFrame, position.x, position.y, size.x, size.y);
	}
	
	public void fadeOut(ArrayList<Sound> sounds) {
		sounds.get(0).setVolume(sID, volume);
		volume -= 0.03f;
	}

}
