package com.me.rocketbird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class DynamicSlider {
	
	Vector2 position1, position2, size, position, sliderSize;
	float valueNorm = 0.0f;
	int p1_x, p1_y, s1_x, s1_y, s2_x, s_x;
	boolean active;
	Texture texture1, texture2;
	Sprite sprite1, sprite2;
	int height;
	float clickTimer = 0.0f;
	
	int rate;
	
	public DynamicSlider(Vector2 position1, Vector2 position2, Vector2 size, String textureLoc1, String textureLoc2, int rate, boolean active, float scr_height, Vector2 sliderPos1, Vector2 sliderPos2, Vector2 sliderSize) {
		
		this.height = (int) scr_height;
		this.active = active;
		this.position1 = position1;
		this.position2 = position2;
		this.sliderSize = sliderSize;
		this.size = size;
		this.rate = rate;
		s1_x = (int) sliderPos1.x;
		s2_x = (int) sliderPos2.x;
		s1_y = (int) sliderPos1.y;
		s_x = (int) sliderPos1.x;
		
		texture1 = new Texture(Gdx.files.internal(textureLoc1));
		texture2 = new Texture(Gdx.files.internal(textureLoc2));
		sprite1 = new Sprite(texture1);
		sprite2 = new Sprite(texture2);
		this.position = position1;
		p1_x = (int) position1.x;
		p1_y = (int) position1.y;
		
		this.valueNorm = 0.0f;
			
	}
	
	public void render(SpriteBatch batch) {
		if(active) {
			position.x = approachExpo(position.x,position2.x,rate);
			position.y = approachExpo(position.y,position2.y,rate);
			s_x = (int) approachExpo(s_x, s1_x + (valueNorm*(s2_x - s1_x)), rate);
		} else {
			position.x = approachExpo(position.x,p1_x,rate);
			position.y = approachExpo(position.y,p1_y,rate);
		}
		// Draw Background
		batch.draw(sprite1, position.x, position.y, size.x, size.y);
		
		if(Math.abs(position.x - position2.x) < 20) {
			// Draw Slider
			batch.draw(sprite2, s_x, s1_y, sliderSize.x, sliderSize.y);
		}
	}
	
	public void reset() {
		this.position.x = p1_x;
		this.position.y = p1_y;
		this.s_x = s1_x;
	}

	// Approach target value smoothly
	public float approachExpo(float value, float target, float rate) {
		if(target > value) {
			return value + (target-value)/rate;
		} else if(target < value) {
			return value - (value-target)/rate;
		} else {
			return value;
		}
	}
	
	public float sliderValue() {
		
		int touchX = Gdx.input.getX();
		int touchY = height - Gdx.input.getY();
		
		int boundsLeft = (int) (s1_x);
		int boundsRight = (int) (s2_x + sliderSize.x);
		int boundsTop = (int) (s1_y + sliderSize.y);
		int boundsBottom = (int) (s1_y);
		
		// Check if user has clicked within bounds
		if((touchX > boundsLeft) && (touchX < boundsRight) && (touchY > boundsBottom) && (touchY < boundsTop)) {
			this.valueNorm = ((float) (touchX - s1_x))/((float) (s2_x - s1_x));
			if(this.valueNorm > 1) {
				this.valueNorm = 1;
			} else if(this.valueNorm < 0) {
				this.valueNorm = 0;
			}
		}
		return this.valueNorm;
		
	}

}
