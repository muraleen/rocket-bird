package com.me.rocketbird;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

import org.omg.CORBA_2_3.portable.OutputStream;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.files.FileHandle;

public class LocalData implements Serializable {

	private static final long serialVersionUID = 1L;
	
	int[] Data;
	
	public LocalData(int[] Data) {
		this.Data = Data;
	}
	
	public static void saveData(LocalData data, String fileString) throws IOException {
		FileHandle file = Gdx.files.local(fileString);
		// LocalData savedData = null;
		OutputStream out = null;
		try {
			file.writeBytes(serialize(data), false);
		} catch (Exception ex) {
			System.out.println(ex.toString());
		} finally {
			if(out != null) {
				try {
					out.close();
				} catch(Exception ex) {}
			}
		}
	}
	
	public static LocalData readData(String fileString) throws IOException, ClassNotFoundException {
		LocalData data = null;
		FileHandle file = Gdx.files.local(fileString);
		
		data = (LocalData) deserialize(file.readBytes());
		
		return data;
	}
	
	private static byte[] serialize(Object obj) throws IOException {
		ByteArrayOutputStream b = new ByteArrayOutputStream();
		ObjectOutputStream o = new ObjectOutputStream(b);
		o.writeObject(obj);
		return b.toByteArray();
	}
	
	public static Object deserialize(byte[] bytes) throws IOException, ClassNotFoundException {
		ByteArrayInputStream b = new ByteArrayInputStream(bytes);
		ObjectInputStream o = new ObjectInputStream(b);
		return o.readObject();
	}
	
}
