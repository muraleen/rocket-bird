package com.me.rocketbird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;

public class DispDigits {

	Vector2 position, size;
	
	Texture digits;
	Texture ft;
	Sprite ftSprite;
	TextureRegion[] frames;
	boolean FromLeft = true, showZero = false;
	
	String textureLoc, suffixLoc;
	
	int[] digit = {10,10,10,10,10,10,10,10,10,10,10,10,10};
	
	public DispDigits(Vector2 position, Vector2 size, String textureLoc, String suffixLoc, boolean FromLeft, boolean showZero) {

		this.position = position;
		this.size = size;
		digits = new Texture(Gdx.files.internal(textureLoc));
		TextureRegion[][] tmp = TextureRegion.split(digits, digits.getWidth()/10, digits.getHeight());
		frames = new TextureRegion[10];
		this.FromLeft = FromLeft;
		this.showZero = showZero;
		
		ft = new Texture(Gdx.files.internal(suffixLoc));
		ftSprite = new Sprite(ft);
		
		int index = 0;
		
		for(int i = 0; i < 10; i++) {
				frames[index++] = tmp[0][i];
		}
	}
	// Equations from Right: X_i = X_0 + Wd(i-l).
	
	public void display(SpriteBatch batch, int n) { // Similar to render function
		
		// Find number of digits to display
		int length = (int) (Math.log10(n));
		
		if((n < 1) && (showZero)) {
			
			length = 0;
			batch.draw(frames[0], position.x, position.y, size.x, size.y);
			
		} else {
		
			for(int i = 0; i <= length; i++) {
				digit[i] = (int) (n/Math.pow(10,(length - i)));
				for(int j = 0; j < i; j++) {
					digit[i] -= (int) ((digit[j]*Math.pow(10,(length - j))))/Math.pow(10,(length - i));
				}
				
				if(FromLeft){
					batch.draw(frames[digit[i]], position.x + (i*size.x*3)/4, position.y, size.x, size.y);
				} else {
					batch.draw(frames[digit[i]], position.x + (((size.x*3)/4)*(i-length - 1)), position.y, size.x, size.y);
				}
			
			}
		
		}
		
		if(FromLeft){
			batch.draw(ftSprite,position.x + 5 + (size.x*3*(length+1))/4, position.y, size.x, size.x);
		} else {
			batch.draw(ftSprite,position.x - ((length + 1)*((size.x*3)/4)) - size.y, position.y, size.y, size.y);
		}
				
	}
	
}
