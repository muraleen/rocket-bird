package com.me.rocketbird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class DynamicCheckButton {
	
	Vector2 position1, position2, size, position;
	int p1_x, p1_y;
	boolean active;
	boolean checked;
	Texture texture1, texture2;
	Sprite sprite1, sprite2;
	int height;
	float clickTimer = 0.0f;
	
	int rate;
	
	public DynamicCheckButton(Vector2 position1, Vector2 position2, Vector2 size, String textureLoc1, String textureLoc2, int rate, boolean active, boolean useTexture1, float scr_height) {
		
		this.height = (int) scr_height;
		this.checked = useTexture1;
		this.active = active;
		this.position1 = position1;
		this.position2 = position2;
		this.size = size;
		this.rate = rate;
		
		texture1 = new Texture(Gdx.files.internal(textureLoc1));
		texture2 = new Texture(Gdx.files.internal(textureLoc2));
		sprite1 = new Sprite(texture1);
		sprite2 = new Sprite(texture2);
		this.position = position1;
		p1_x = (int) position1.x;
		p1_y = (int) position1.y;
			
	}
	
	public void render(SpriteBatch batch) {
		if(active) {
			position.x = approachExpo(position.x,position2.x,rate);
			position.y = approachExpo(position.y,position2.y,rate);
		} else {
			position.x = approachExpo(position.x,p1_x,rate);
			position.y = approachExpo(position.y,p1_y,rate);
		}
		if(checked) {
			batch.draw(sprite1, position.x, position.y, size.x, size.y);
		} else {
			batch.draw(sprite2, position.x, position.y, size.x, size.y);
		}
	}
	
	public void reset() {
		this.position.x = p1_x;
		this.position.y = p1_y;
	}

	// Approach target value smoothly
	public float approachExpo(float value, float target, float rate) {
		if(target > value) {
			return value + (target-value)/rate;
		} else if(target < value) {
			return value - (value-target)/rate;
		} else {
			return value;
		}
	}
	
	public boolean isClicked() {
		if(clickTimer > 0.1f) {
			clickTimer = 0f;
			if(Gdx.input.isTouched()) {
				int touchX = Gdx.input.getX();
				int touchY = height - Gdx.input.getY();
				if((touchX > position.x) && (touchX < position.x + size.x) && (touchY > position.y) && (touchY < position.y + size.y)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			clickTimer += Gdx.graphics.getDeltaTime();
			return false;
		}
	}

}
