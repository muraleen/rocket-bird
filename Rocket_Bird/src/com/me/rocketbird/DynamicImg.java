package com.me.rocketbird;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;

public class DynamicImg {
	
	Vector2 position1, position2, size, position;
	int p1_x, p1_y;
	boolean active;
	String textureLoc;
	Texture texture;
	Sprite sprite;
	
	int rate;
	
	public DynamicImg(Vector2 position1, Vector2 position2, Vector2 size, String textureLoc, int rate, boolean active) {
		
		this.active = active;
		this.position1 = position1;
		this.position2 = position2;
		this.size = size;
		this.rate = rate;
		
		texture = new Texture(Gdx.files.internal(textureLoc));
		sprite = new Sprite(texture);
		this.position = position1;
		p1_x = (int) position1.x;
		p1_y = (int) position1.y;
			
	}
	
	public void render(SpriteBatch batch) {
		if(active) {
			if(rate > 50) {
				position.y -= rate - 50;
			} else {
				position.x = approachExpo(position.x,position2.x,rate);
				position.y = approachExpo(position.y,position2.y,rate);
			}
		} else {
			if(rate > 50) {
				position.y += 10;
			} else {
				position.x = approachExpo(position.x,p1_x,rate);
				position.y = approachExpo(position.y,p1_y,rate);
			}
		}
		batch.draw(sprite, position.x, position.y, size.x, size.y);
	}
	
	public void draw(SpriteBatch batch) {
		batch.draw(sprite, position.x, position.y, size.x, size.y);
	}
	
	public void reset() {
		this.position.x = p1_x;
		this.position.y = p1_y;
	}

	// Approach target value smoothly
	public float approachExpo(float value, float target, float rate) {
		if(target > value) {
			return value + (target-value)/rate;
		} else if(target < value) {
			return value - (value-target)/rate;
		} else {
			return value;
		}
	}
}
