#!/bin/bash


for img in *.png; do

	convert "$img" -rotate 180 "Crop-Resized/RESIZED_$img";

done

cd "Crop-Resized";

montage *.png -geometry '+0+0'  "compiled.png"
