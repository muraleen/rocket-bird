#!/bin/bash


for img in *.png; do

	convert "$img" -crop '1080x1080+420x+0' -resize '512x512!' "Crop-Resized/RESIZED_$img";

done

cd "Crop-Resized";

montage *.png -tile '8x' -geometry '+0+0'  "compiled.png"
