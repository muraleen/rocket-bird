#!/bin/bash


for img in *.png; do

	convert "$img" -crop '760x380+102x+80' -resize '512x256!' "Crop-Resized/RESIZED_$img";

done

cd "Crop-Resized";

montage *.png -geometry '+0+0'  "compiled.png"
