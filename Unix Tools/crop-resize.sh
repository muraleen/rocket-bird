#!/bin/bash

for img in *.png; do

	convert "$img" -crop '680x540+132x+0' "CROPPED_$img";
	convert "CROPPED_$img" -resize '256x256' "RESIZED_$img";

done
